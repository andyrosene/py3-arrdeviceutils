from setuptools import find_packages, setup

setup(
    name='arrdeviceutils',
    version='2020.6.3',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.8',
    install_requires=[
        'arrelevator @ git+ssh://git@gitlab.com/andyrosene/py3-arrelevator.git#egg=arrelevator',
        'arrfs @ git+ssh://git@gitlab.com/andyrosene/py3-arrfs.git#egg=arrfs',
    ],
)
