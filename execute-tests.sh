#!/bin/bash
cd src

echo "
##############################################################################
# ALL TESTS
##############################################################################
"
python3.8 -m unittest discover -v -s ../tests
if [[ "$?" != "0" ]]; then
  exit $?
else
  echo ""
  echo "Result: $?"
  echo ""
fi

echo "
==============================================================================
TESTING COMPLETED
"
