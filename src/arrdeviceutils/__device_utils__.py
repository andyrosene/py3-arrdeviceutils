from arrelevator import elevate
from arrfs import *
from os import system
from subprocess import run
from typing import List

__all__ = [
    'verify_disk_device_connected',
    'get_disk_partitions',
    'get_partition_index',
    'get_partition_mountpoint',
    'unmount_partition_at_mountpoint',
    'unmount_all_disk_partitions',
    'get_partition_device_path_by_num',
    'mount_partition',
    'get_num_partitions',
    'delete_all_partitions',
    'request_target_disk_device',
]


def verify_disk_device_connected(disk_device_path: str):
    """Verify the disk with the given device path is connected to the computer.

    Args:
        disk_device_path: The absolute path to the disk device.

    Raises:
        AssertionError: If something goes wrong when checking the specified device.
    """
    print('> Verifying disk is connected at', disk_device_path)
    r1 = run(['lsblk', '-lndo', 'TYPE', disk_device_path], capture_output=True)
    run(['grep', '-w', 'disk'], input=r1.stdout, check=True, capture_output=True)


def get_disk_partitions(disk_device_path: str) -> List[str]:
    """Get the list of partitions available on the disk with the given device path.

    Args:
        disk_device_path: the device path of the given disk.

    Returns:
        List[str]: The list of partitions available on the given disk.

    Raises:
        AssertionError: Something went wrong trying to get the disk partitions.
    """
    print('> Getting partitions for', disk_device_path)
    r1 = run(['lsblk', '-lnpo', 'NAME,TYPE', disk_device_path], capture_output=True)
    r2 = run(['grep', '-w', 'part$'], input=r1.stdout, capture_output=True)
    r3 = run(['awk', '{print $1}'], input=r2.stdout, check=True, capture_output=True)
    return r3.stdout.strip().decode('utf-8').splitlines()


def get_partition_index(partition_device_path: str) -> str:
    print('> Getting the index for', partition_device_path)
    r1 = run(['lsblk', '-lno', 'MAJ:MIN', partition_device_path], check=True, capture_output=True)
    return r1.stdout.strip().decode('utf-8').split(':')[1]


def get_partition_mountpoint(partition_device_path: str) -> str:
    r1 = run(['lsblk', '-lno', 'MOUNTPOINT', partition_device_path], capture_output=True, check=True)
    return r1.stdout.strip().decode('utf-8')


def unmount_partition_at_mountpoint(mount_dir: ADir):
    """Un-mount the partition mounted at the given mount directory.

    fixme - This should be removed in favor of removal of device using it's path. This is susceptible to issues where
     the name contains odd characters (e.g. \040 rather than space).

    Args:
        mount_dir: The given mount directory.

    Raises:
        CalledProcessError: Something went wrong unmounting the partition mounted to the given directory.
    """
    __synchronize_cached_writes()
    print('Un-mounting device mounted at', mount_dir.path)
    elevate('Elevated permissions are required for un-mounting partitions')
    run(['sudo', 'umount', mount_dir.path], check=True, capture_output=True)


def unmount_all_disk_partitions(disk_device_path: str):
    print('Un-mounting all mounted partitions for', disk_device_path)
    r1 = run(['cat', '/proc/mounts'], capture_output=True)
    r2 = run(['grep', disk_device_path], input=r1.stdout, capture_output=True)
    r3 = run(['awk', '{print $1}'], input=r2.stdout, capture_output=True)

    for partition_mount_point in r3.stdout.strip().decode('utf-8').splitlines():
        print('Unmounting', partition_mount_point)
        elevate('Elevated permissions are required for un-mounting partitions')
        run('sudo umount {}'.format(partition_mount_point), shell=True)


def get_partition_device_path_by_num(disk_device_path: str, partition_num: int) -> str:
    r1 = run(['lsblk', '-lnpo', 'NAME,MAJ:MIN,TYPE', disk_device_path], capture_output=True)
    r2 = run(['grep', '-w', 'part$'], input=r1.stdout, capture_output=True)
    r3 = run(['awk', '{print $1 " " $2}'], input=r2.stdout, capture_output=True)
    r4 = run(['grep', ':{}$'.format(partition_num)], input=r3.stdout, capture_output=True)
    r5 = run(['awk', '{print $1}'], input=r4.stdout, check=True, capture_output=True)

    if r5.stdout.strip() == b'':
        raise AssertionError('No such partition.')

    return r5.stdout.strip().decode('utf-8')


def mount_partition(device_path: str, partition_type: str, mount_dir: ADir):
    """Mount the partition with the given device path to the given mount directory using the given partitions type. On
    error, terminate execution.

    Notes:
        - If the given mount directory does not exist, it will automatically be created.

    Args:
        device_path: The device path for the partition being mounted.
        partition_type: The type of partition being mounted (eg vfat or ext4).
        mount_dir: The given mount directory.

    Raises:
        CalledProcessError: If something goes wrong running the mount command.
    """
    mount_dir.create()  # Create the mount directory if it doesn't exist.

    print('Mounting', device_path, 'to', mount_dir.path, 'as', partition_type)
    elevate('Partition mounting requires elevated permissions')
    run('sudo mount -o rw -t {} "{}" "{}"'.format(partition_type, device_path, mount_dir.path), shell=True, check=True)


def get_num_partitions(disk_device_path: str) -> int:
    print('> Getting the number of partitions on', disk_device_path)
    r1 = run(['lsblk', '-lnpo', 'NAME,TYPE', disk_device_path], capture_output=True)
    r2 = run(['grep', '-w', 'part$'], input=r1.stdout, capture_output=True)
    r3 = run(['awk', '{print $1}'], input=r2.stdout, check=True, capture_output=True)
    return len(r3.stdout.strip().splitlines())


def delete_all_partitions(disk_device_path: str):
    print('Deleting all partitions on', disk_device_path)
    elevate('Deleting partitions requires elevated permissions')

    run('sudo hdparm -z {}'.format(disk_device_path), shell=True)

    r1 = run(['sudo', 'gdisk', '-l', disk_device_path], capture_output=True)
    r2 = run(['grep', '^Number  ', '-A100'], input=r1.stdout, capture_output=True)
    r3 = run(['tail', '-n', '+2'], input=r2.stdout, capture_output=True)
    r4 = run(['awk', '{print $1}'], input=r3.stdout, capture_output=True)

    for i in r4.stdout.strip().decode('utf-8').splitlines():
        print('\tDeleting Partition', i)

        r1 = run(
            'sudo gdisk -l {} | grep \'^Number  \' -A100 | tail -n +2 | wc -l'.format(disk_device_path),
            shell=True,
            capture_output=True
        )
        if r1.stdout.strip() == b'1':
            run('''\
sudo gdisk {} <<EOF
d
w
Y
EOF'''.format(disk_device_path), shell=True)
        else:
            run('''\
sudo gdisk {} <<EOF
d
{}
w
Y
EOF'''.format(disk_device_path, i), shell=True)

    run('sudo hdparm -z {}'.format(disk_device_path), shell=True)


def request_target_disk_device() -> str:
    """Request a target disk from the current user.

    Returns:
        str: The device path of the disk specified by the current user.
    """
    print('')
    r1 = run(['lsblk', '-lnpd'], capture_output=True)
    run(['grep', '-w', 'disk'], input=r1.stdout, check=True)
    print('-' * 80)
    device_path = input('Enter Device Path for target disk: ')
    verify_disk_device_connected(device_path)
    return device_path


def __synchronize_cached_writes():
    """Synchronize cached writes to persistent storage. This **must** be performed prior to **un-mounting**."""
    print('> Synchronizing all cached writes')
    run('sync', shell=True)
